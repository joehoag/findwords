package com.joeh.findwords

import android.content.Context
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*

object WordManager {
    val wordSet = mutableSetOf<String>() // For checking the legality of works
    lateinit var wordList : List<String> // For choosing random words
    lateinit var random : Random

    fun initialize(context: Context) {

        // Read our word file
        var reader: BufferedReader? = null
        try {
            val wordStream = context.assets.open("corncob_lowercase.txt")
            reader = BufferedReader(InputStreamReader(wordStream))
            var line = reader.readLine()
            while (line != null) {
                wordSet.add(line)
                line = reader.readLine()
            }
        }
        finally {
            if(reader != null) reader.close()
        }

        // Make our word list (is this necessary?)
        wordList = wordSet.toList()

        // Set up our random number generator
        random = Random(System.currentTimeMillis())
    }

    fun isValidWord(word: String) : Boolean {
        return wordSet.contains(word)
    }

    fun getRandomSixLetterWord() : String {
        var result = getRandomWord()
        while(result.length != 6) {
            result = getRandomWord()
        }
        return result
    }

    fun getRandomWord() : String {
        val size = wordList.size
        val index = random.nextInt(size)
        return wordList[index]
    }

    fun getAllValidSubwords(word: String) : Set<String> {
        val size = word.length
        val limit = Math.pow(2.0, size.toDouble()).toInt() - 1
        val result = mutableSetOf<String>()

        for(i in 1..limit) {
            // i is binary coded indicator of which letters to include
            val sb = StringBuilder()
            for(index in 0..size-1) {
                if( (i shr index) and 1 == 1) sb.append(word[index])
            }

            val subword = sb.toString()
            getAllValidPermutations(subword, result)
        }

        return result
    }

    fun getAllValidPermutations(word: String, results: MutableSet<String>) {
        results.addAll(permutationHelper(word).filter {x -> wordSet.contains(x)}.filter {x -> x.length >= 3})
    }

    private fun permutationHelper(word: String) : Set<String> {
        val result = mutableSetOf<String>()
        val size = word.length

        if(size == 1) {
            result.add(word)
            return result
        }

        for(leadIndex in 0..size-1) {
            var leadChar = word[leadIndex]
            var subword = word.substring(0,leadIndex) + word.substring(leadIndex+1)
            val subresult = permutationHelper(subword)
            for(subword in subresult) result += (leadChar + subword)
        }

        return result
    }
}