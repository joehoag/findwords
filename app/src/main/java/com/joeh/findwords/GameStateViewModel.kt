package com.joeh.findwords

import android.os.Parcelable
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class WordState (
    val word: String,
    var submitted: Boolean
) : Parcelable

class GameStateViewModel(val savedStateHandle: SavedStateHandle) : ViewModel() {
    val gameWord : MutableLiveData<String> = MutableLiveData<String>()
    val subWordStates : MutableLiveData<List<WordState>> = MutableLiveData<List<WordState>>()

    init {
        val _gameWord : String? = savedStateHandle["gameword"]
        val _subWordStates : List<WordState>? = savedStateHandle["wordstates"]
        if(_gameWord != null && _subWordStates != null) {
            gameWord.value = _gameWord
            subWordStates.value = _subWordStates
        }
        else {
            newGame()
        }
    }

    fun newGame() {
        val _gameWord = WordManager.getRandomSixLetterWord()
        gameWord.value = _gameWord
        val _subWordStates = WordManager.getAllValidSubwords(_gameWord)
            .map { x -> WordState(x, false) }
        subWordStates.value = _subWordStates

        savedStateHandle.set("gameword", _gameWord)
        savedStateHandle.set("wordstates", _subWordStates)
    }

    fun updateWordState(word: String, submitted: Boolean) {
        val state = subWordStates.value!!.filter { s -> s.word == word}.firstOrNull()
        if(state != null) {
            state.submitted = submitted
            subWordStates.value = subWordStates.value
            savedStateHandle.set("wordstates", subWordStates.value)
        }
    }

    fun getWordState(word: String) : WordState? {
        return subWordStates.value!!.filter{ s -> s.word == word}.firstOrNull()
    }
}