package com.joeh.findwords

import android.os.Bundle
import android.os.Parcelable
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Button
import android.widget.LinearLayout.LayoutParams
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.children
import androidx.lifecycle.SavedStateViewModelFactory
import kotlinx.android.parcel.Parcelize
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var runningCandidate = "" // Not part of GameStateViewModel for now
    private val viewModel: GameStateViewModel by viewModels {
        SavedStateViewModelFactory(this.application, this)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle item selection
        return when (item.itemId) {
            R.id.new_game -> {
                val dialogBuilder = AlertDialog.Builder(this)
                dialogBuilder
                    .setTitle("New Game")
                    dialogBuilder.setMessage("Abandon game and create new game?")
                    dialogBuilder.setPositiveButton("Yes") { dialogInterface, something ->
                        viewModel.newGame()
                    }
                    dialogBuilder.setNegativeButton("No", null)
                    .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val time1 = System.currentTimeMillis()
        WordManager.initialize(this) // Needs to be moved
        val time2 = System.currentTimeMillis()

        println("WordManager: setup time (ms) = ${time2 - time1}")

        viewModel.gameWord.observe(this, { word ->
            // Set up gameword buttons
            gameword.removeAllViews()
            runningCandidate = ""
            guessText.text = ""
            for(i in 0..word.length-1) {
                val c = word[i]
                val button = Button(this)
                button.text = "" + c.toUpperCase()
                button.tag = c
                button.textSize = 20.toFloat()
                val layoutParams = LayoutParams(0, LayoutParams.WRAP_CONTENT)
                layoutParams.weight = 1.toFloat()
                layoutParams.setMargins(5,5,5, 5)
                button.layoutParams = layoutParams

                button.setOnClickListener { v ->
                    runningCandidate += (v.tag as Char)
                    guessText.text = runningCandidate
                    v.isEnabled = false
                    btnBack.isEnabled = true
                }
                gameword.addView(button)
            }
        })

        viewModel.subWordStates.observe(this, { stateList ->
            sixLetterWords.removeAllViews()
            fiveLetterWords.removeAllViews()
            fourLetterWords.removeAllViews()
            threeLetterWords.removeAllViews()

            for(state in stateList) {
                val subword = state.word
                val layout = when(subword.length) {
                    6 -> sixLetterWords
                    5 -> fiveLetterWords
                    4 -> fourLetterWords
                    else -> threeLetterWords
                }

                val textToDisplay = if(state.submitted) state.word else "___"

                val textView = TextView(this)
                textView.text = textToDisplay
                textView.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
                textView.textSize = 20.toFloat()
                layout.addView(textView)

            }

        })

        // Backspace logic
        btnBack.setOnClickListener { btn ->
            if (!runningCandidate.isEmpty()) {
                val deletedChar =
                    runningCandidate[runningCandidate.length - 1]
                runningCandidate =
                    runningCandidate.take(runningCandidate.length - 1)
                guessText.text = runningCandidate
                if (runningCandidate.isEmpty()) btnBack.isEnabled = false
                val buttonToEnable =
                    gameword.children.filter { child -> child.tag == deletedChar && !child.isEnabled }
                        .firstOrNull()
                buttonToEnable?.isEnabled = true
            }
        }

        btnSubmit.setOnClickListener { btn ->
            val candidate = runningCandidate
            val wordState = viewModel.getWordState(candidate)

            if(wordState == null) {
                Toast.makeText(this,"Invalid word!", Toast.LENGTH_LONG).show()
            }
            else if(wordState.submitted) {
                Toast.makeText(this, "Already guessed", Toast.LENGTH_LONG).show()
            }
            else {
                Toast.makeText(this, "Accepted!", Toast.LENGTH_LONG).show()
                wordState.submitted = true
                viewModel.updateWordState(candidate, true)
                //displaySubwords()
            }

            // Now reset the submission state
            runningCandidate = ""
            btnBack.isEnabled = false
            gameword.children.forEach {child -> child.isEnabled = true}
            guessText.text = ""
        }
    }

}



